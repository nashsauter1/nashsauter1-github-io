---
title: about this blog
permalink: /about/
layout: page
excerpt: This blog was created using Jekyll.
comments: false
---

This blog was created using Jekyll and is primarily written in Markdown. The source code for it is on my Github page in case you want to see what it looks like from the inside. 

**✨Links✨**

- github.com/{{ site.author.github }}
